package org.neylson.videolocadora.services;

import java.util.List;

import javax.persistence.Query;

import org.neylson.videolocadora.entidades.Ator;

public class AtorService extends EntityService<Ator>{

	public AtorService() {
		super(Ator.class);
	}

	public List<Ator> filter(String prefixo) {
		prefixo += "%";
		Query query = entityManager.createQuery("select a from Ator a where a.nome like :prefixo");
		query.setParameter("prefixo", prefixo);
		
		return query.getResultList();
	}
}
