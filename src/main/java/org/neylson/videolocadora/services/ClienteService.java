package org.neylson.videolocadora.services;

import org.neylson.videolocadora.entidades.Cliente;

public class ClienteService extends EntityService<Cliente>{

	public ClienteService() {
		super(Cliente.class);
	}

}
