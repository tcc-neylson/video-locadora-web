package org.neylson.videolocadora.services;

import org.neylson.videolocadora.entidades.Filme;


public class FilmeService extends EntityService<Filme>{

	
	public FilmeService() {
		super(Filme.class);
	}

}
