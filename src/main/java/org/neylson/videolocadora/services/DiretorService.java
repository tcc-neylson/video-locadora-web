package org.neylson.videolocadora.services;

import java.util.List;

import javax.persistence.Query;

import org.neylson.videolocadora.entidades.Diretor;

public class DiretorService extends EntityService<Diretor>{

	
	public DiretorService() {
		super(Diretor.class);
	}
	
	public List<Diretor> filter(String prefixo) {
		prefixo += "%";
		Query query = entityManager.createQuery("select d from Diretor d where d.nome like :prefixo");
		query.setParameter("prefixo", prefixo);
		
		return query.getResultList();
	}

}
