package org.neylson.videolocadora.services;

import org.neylson.videolocadora.entidades.Categoria;

public class CategoriaService extends EntityService<Categoria>{

	
	public CategoriaService() {
		super(Categoria.class);
	}

}
