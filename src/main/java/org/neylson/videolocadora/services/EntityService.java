package org.neylson.videolocadora.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import org.neylson.videolocadora.entidades.PersistentEntity;

public abstract class EntityService<T extends PersistentEntity> {

	@PersistenceContext
	protected EntityManager entityManager;
	
	private Class<T> entityClass;
	
	public EntityService(Class<T> entityClass) {
		this.entityClass = entityClass;
	}
	
	public void salvar(T entity) {
		entityManager.persist(entity);
	}
	
	public T buscar(Long id) {
		return (T) entityManager.find(entityClass, id);
	}

	public void remover(T entity) {
		entityManager.remove(entity);		
	}

	public void atualizar(T entity) {
		entityManager.merge(entity);	
	}
	
	public List<T> list() {
		CriteriaQuery<T> cq = (CriteriaQuery<T>) entityManager.getCriteriaBuilder().createQuery(entityClass);
		cq.select(cq.from(entityClass));
		
		return entityManager.createQuery(cq).getResultList();
	}
}
