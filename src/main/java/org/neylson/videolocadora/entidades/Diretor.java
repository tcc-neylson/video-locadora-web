package org.neylson.videolocadora.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Diretor implements Serializable, PersistentEntity {


	private static final long serialVersionUID = 726169551250680047L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@NotEmpty
	@Length(min=5)
	private String nome;
	
	private boolean ganhadorDoOscar;
	
	@OneToMany(mappedBy="diretor")
	private List<Filme> filmes;

	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isGanhadorDoOscar() {
		return ganhadorDoOscar;
	}

	public void setGanhadorDoOscar(boolean ganhadorDoOscar) {
		this.ganhadorDoOscar = ganhadorDoOscar;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
}
