package org.neylson.videolocadora.entidades;

public interface PersistentEntity {

	Long getId();
	void setId(Long id);
}
