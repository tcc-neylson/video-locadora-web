package org.neylson.videolocadora.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.br.CPF;


@Entity
public class Cliente implements Serializable, PersistentEntity{

	public enum Sexo {MASCULINO, FEMININO};
	public enum EstadoCivil {SOLTEIRO, CASADO};

	private static final long serialVersionUID = 7200649932204497274L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank
	@Length(min=5)
	private String nome;
	
	@CPF
	private String cpf;
	
	@Length(min=3)
	private String rg;
	
	@Temporal(TemporalType.DATE)
	@Past
	@NotNull
	private Date dataDeNascimento;
	
	@Enumerated(EnumType.STRING)
	@NotNull
	private Sexo sexo;
	
	@Enumerated(EnumType.STRING)
	@NotNull
	private EstadoCivil estadoCivil;
	
	@Embedded
	private Endereco endereco = new Endereco();

	@OneToMany(mappedBy="cliente", cascade=CascadeType.REMOVE)
	private List<Reserva> reservas;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public Date getDataDeNascimento() {
		return dataDeNascimento;
	}

	public void setDataDeNascimento(Date dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (!(obj instanceof Cliente))
            return false;
        
		Cliente other = (Cliente) obj;
		if(this.id == null || other.getId() == null){
			return false;
		}
		
		return this.getId().equals(other.getId());
	}
	
	
	@Override
	public int hashCode() {
		return (int) ((id == null) ? 0 : id);
	}
	
}
