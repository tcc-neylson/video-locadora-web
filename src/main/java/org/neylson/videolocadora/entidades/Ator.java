package org.neylson.videolocadora.entidades;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@Entity
public class Ator implements Serializable, PersistentEntity{


	private static final long serialVersionUID = -1543428885065935429L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank
	@Length(min=5)
	private String nome;
	
	@Min(0)
	private int quantidadeDeOscars;
	
	@ManyToMany
	private List<Filme> filmes;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getQuantidadeDeOscars() {
		return quantidadeDeOscars;
	}

	public void setQuantidadeDeOscars(int quantidadeDeOscars) {
		this.quantidadeDeOscars = quantidadeDeOscars;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	
}
