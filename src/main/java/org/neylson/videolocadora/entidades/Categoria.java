package org.neylson.videolocadora.entidades;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Categoria implements PersistentEntity, Serializable {

	private static final long serialVersionUID = 7681074587963951042L;

	public enum Tipo {
		LANCAMENTO, PROMOCAO, CATALOGO
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.STRING)
	private Tipo tipo;
	private double valor;
	private int diasDeLocacao;

	public Categoria() {
	}

	public Categoria(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public int getDiasDeLocacao() {
		return diasDeLocacao;
	}

	public void setDiasDeLocacao(int diasDeLocacao) {
		this.diasDeLocacao = diasDeLocacao;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		
		if(obj instanceof Categoria){
			return ((Categoria) obj).getId().equals(this.id);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return (int) ((id == null) ? 0 : id);
	}
}
