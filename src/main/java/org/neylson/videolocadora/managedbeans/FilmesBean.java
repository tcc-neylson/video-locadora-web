package org.neylson.videolocadora.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.neylson.videolocadora.entidades.Filme;
import org.neylson.videolocadora.infra.MessagesHelper;
import org.neylson.videolocadora.services.FilmeService;

@Named
@ViewScoped
public class FilmesBean implements Serializable {

	private static final long serialVersionUID = 2128403779215074722L;

	@Inject
	private transient MessagesHelper mensagens;
	@Inject
	private transient FilmeService filmeService;
	private Filme filme = new Filme();
	private boolean editar = false;
	private List<Filme> filmesFiltrados;

	@PostConstruct
	public void init(){
	}
	
	public List<Filme> getAll() {
		return filmeService.list();
	}

	@Transactional
	public void remover(Long id) {
		Filme filme = filmeService.buscar(id);
		filmeService.remover(filme);

		mensagens.addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Filme removido com sucesso!", null));
	}

	@Transactional
	public void salvar() {
		filmeService.salvar(filme);

		mensagens.addMessage(new FacesMessage("Filme cadastrado com sucesso!"));
		limpar();
	}

	public void limpar() {
		filme = new Filme();		
	}

	public void carregar(Long id) {
		filme = filmeService.buscar(id);
		setEditar(true);
	}

	@Transactional
	public void atualizar() {
		filmeService.atualizar(filme);
		setEditar(false);
	}

	public boolean getEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}

	public Filme getFilme() {
		return filme;
	}

	public void setFilme(Filme filme) {
		this.filme = filme;
	}

	public List<Filme> getFilmesFiltrados() {
		return filmesFiltrados;
	}

	public void setFilmesFiltrados(List<Filme> filmesFiltrados) {
		this.filmesFiltrados = filmesFiltrados;
	}
	
}
