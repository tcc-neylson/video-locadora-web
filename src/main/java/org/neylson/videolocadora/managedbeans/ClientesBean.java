package org.neylson.videolocadora.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.neylson.videolocadora.entidades.Cliente;
import org.neylson.videolocadora.entidades.Cliente.EstadoCivil;
import org.neylson.videolocadora.entidades.Cliente.Sexo;
import org.neylson.videolocadora.infra.MessagesHelper;
import org.neylson.videolocadora.services.ClienteService;

@Named
@ViewScoped
public class ClientesBean implements Serializable {
	

	private static final long serialVersionUID = 4434409131043627083L;
	
	private Cliente cliente = new Cliente();
        
	@Inject
	private transient MessagesHelper mensagens;
        
	@Inject
	private transient ClienteService clienteService;
	
	private boolean editar = false;

	
	public List<Cliente> getAll() {
		return clienteService.list(); 
	}
	
	@Transactional
	public void remover(Long id) {
		Cliente cliente = clienteService.buscar(id);
		clienteService.remover(cliente);

		mensagens.addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Cliente removido com sucesso!", null));
	}

	@Transactional
	public void salvar() {
		clienteService.salvar(cliente);

		mensagens.addMessage(new FacesMessage("Cliente cadastrado com sucesso!"));
		limpar();
	}
	
	public void limpar() {
		cliente = new Cliente();
	}
	
	public void carregar(Long id) {
		cliente = clienteService.buscar(id);
		setEditar(true);
	}

	@Transactional
	public void atualizar() {
		clienteService.atualizar(cliente);
		setEditar(false);
	}
	
	
	public boolean getEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	
	public SelectItem[] getEstadosCivis(){
		EstadoCivil[] estadosCivis = Cliente.EstadoCivil.values();
		SelectItem[] items = new SelectItem[estadosCivis.length];
		
		int i = 0;
		for (EstadoCivil estadoCivil : estadosCivis) {
			items[i++] = new SelectItem(estadoCivil, estadoCivil.name());
		}
		
		return items;
	}
	
	public SelectItem[] getSexos(){
		Sexo[] sexos = Cliente.Sexo.values();
		SelectItem[] items = new SelectItem[sexos.length];
		
		int i = 0;
		for (Sexo sexo : sexos) {
			items[i++] = new SelectItem(sexo, sexo.name());
		}
		
		return items;
	}
	
		
	
}
