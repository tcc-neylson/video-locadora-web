package org.neylson.videolocadora.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.neylson.videolocadora.entidades.Categoria;
import org.neylson.videolocadora.entidades.Categoria.Tipo;
import org.neylson.videolocadora.infra.MessagesHelper;
import org.neylson.videolocadora.services.CategoriaService;

@Named
@ViewScoped
public class CategoriaBean implements Serializable {


	private static final long serialVersionUID = -4932573275796631963L;
	@Inject
	private transient MessagesHelper mensagens;
	@Inject
	private transient CategoriaService categoriaService;
	private Categoria categoria = new Categoria();
	private boolean editar = false;

	
	public List<Categoria> getAll() {
		return categoriaService.list();
	}

	@Transactional
	public void remover(Long id) {
		Categoria categoria = categoriaService.buscar(id);
		categoriaService.remover(categoria);

		mensagens.addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Categoria removida com sucesso!", null));
	}

	@Transactional
	public void salvar() {
		categoriaService.salvar(categoria);

		mensagens.addMessage(new FacesMessage("Categoria cadastrada com sucesso!"));
		limpar();
	}

	public void limpar() {
		categoria = new Categoria();		
	}

	public void carregar(Long id) {
		categoria = categoriaService.buscar(id);
		setEditar(true);
	}

	@Transactional
	public void atualizar() {
		categoriaService.atualizar(categoria);
		setEditar(false);
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public boolean getEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}
	
	public SelectItem[] getTipos(){
		
		SelectItem[] items = new SelectItem[Categoria.Tipo.values().length];
		
		int i = 0;
		for (Tipo tipo : Categoria.Tipo.values()) {
			items[i++]= new SelectItem(tipo, tipo.name());
		}
		
		return items;
	}

}
