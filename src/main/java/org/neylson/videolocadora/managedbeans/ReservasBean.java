package org.neylson.videolocadora.managedbeans;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.neylson.videolocadora.entidades.Reserva;
import org.neylson.videolocadora.infra.MessagesHelper;
import org.neylson.videolocadora.services.ReservaService;

@Named
@ViewScoped
public class ReservasBean implements Serializable {

	
	private static final long serialVersionUID = -4775903808611473785L;
	
	@Inject
	private transient MessagesHelper mensagens;
	@Inject
	private transient ReservaService reservaService;
	
	private Reserva reserva = new Reserva();
	private boolean editar = false;
	
	private List<Reserva> reservasFiltradas;

	@PostConstruct
	public void init(){
	}
	
	public List<Reserva> getAll() {
		return reservaService.list();
	}

	@Transactional
	public void remover(Long id) {
		Reserva reserva = reservaService.buscar(id);
		reservaService.remover(reserva);

		mensagens.addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Reserva removida com sucesso!", null));
	}

	@Transactional
	public void salvar() {
		Date horario = Calendar.getInstance().getTime();
		reserva.setHorario(horario);
		reservaService.salvar(reserva);

		mensagens.addMessage(new FacesMessage("Reserva realizada com sucesso!"));
		limpar();
	}

	public void limpar() {
		reserva = new Reserva();
	}

	public void carregar(Long id) {
		reserva = reservaService.buscar(id);
		setEditar(true);
	}

	@Transactional
	public void atualizar() {
		reservaService.atualizar(reserva);
		setEditar(false);
		mensagens.addMessage(new FacesMessage("Reserva  com sucesso!"));
	}

	public boolean getEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}

	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}
	
	public List<Reserva> getReservasFiltradas() {
		return reservasFiltradas;
	}

	public void setReservasFiltradas(List<Reserva> reservasFiltradas) {
		this.reservasFiltradas = reservasFiltradas;
	}
}
