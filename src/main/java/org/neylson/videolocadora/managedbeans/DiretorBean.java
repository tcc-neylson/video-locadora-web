package org.neylson.videolocadora.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.neylson.videolocadora.entidades.Diretor;
import org.neylson.videolocadora.infra.MessagesHelper;
import org.neylson.videolocadora.services.DiretorService;


@Named
@ViewScoped
public class DiretorBean implements Serializable{


	private static final long serialVersionUID = -4932573275796631963L;
	@Inject
	private transient MessagesHelper mensagens;
	@Inject
	private transient DiretorService diretorService;
	private Diretor diretor;
	private boolean editar = false;
	private List<Diretor> diretoresFiltrados;

	@PostConstruct
	public void init(){
	}
	
	public List<Diretor> getAll() {
		return diretorService.list();
	}

	@Transactional
	public void remover(Long id) {
		Diretor diretor = diretorService.buscar(id);
		diretorService.remover(diretor);

		mensagens.addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Diretor removido com sucesso!", null));
	}

	@Transactional
	public void salvar() {
		diretorService.salvar(diretor);

		mensagens.addMessage(new FacesMessage("Diretor cadastrado com sucesso!"));
		limpar();
	}

	public void limpar() {
		diretor = new Diretor();		
	}

	public void carregar(Long id) {
		diretor = diretorService.buscar(id);
		setEditar(true);
	}

	@Transactional
	public void atualizar() {
		diretorService.atualizar(diretor);
		setEditar(false);
	}

	public Diretor getDiretor() {
		return diretor;
	}

	public void setDiretor(Diretor diretor) {
		this.diretor = diretor;
	}

	public boolean getEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}

	public List<Diretor> getDiretoresFiltrados() {
		return diretoresFiltrados;
	}

	public void setDiretoresFiltrados(List<Diretor> diretoresFiltrados) {
		this.diretoresFiltrados = diretoresFiltrados;
	}
	
	public List<Diretor> completaDiretores(String prefixo) {
		return diretorService.filter(prefixo);
	}
}
