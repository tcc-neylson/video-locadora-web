package org.neylson.videolocadora.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.neylson.videolocadora.entidades.Ator;
import org.neylson.videolocadora.infra.MessagesHelper;
import org.neylson.videolocadora.services.AtorService;

@Named
@ViewScoped
public class AtoresBean implements Serializable {
	
	private static final long serialVersionUID = -735873575521969041L;

	private Ator ator;
	@Inject
	private transient MessagesHelper mensagens;
	@Inject
	private transient AtorService atorService;
	private boolean editar = false;
	private List<Ator> atoresFiltrados;

	
	public List<Ator> getAll() {
		return atorService.list(); 
	}
	
	@Transactional
	public void remover(Long id) {
		Ator ator = atorService.buscar(id);
		atorService.remover(ator);

		mensagens.addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Ator removido com sucesso!", null));
	}

	@Transactional
	public void salvar() {
		atorService.salvar(ator);

		mensagens.addMessage(new FacesMessage("Ator cadastrado com sucesso!"));
		limpar();
	}
	
	public void limpar() {
		ator = new Ator();
	}
	
	public void carregar(Long id) {
		ator = atorService.buscar(id);
		setEditar(true);
	}

	@Transactional
	public void atualizar() {
		atorService.atualizar(ator);
		setEditar(false);
	}
	
	public Ator getAtor() {
		return ator;
	}

	public void setAtor(Ator ator) {
		this.ator = ator;
	}

	public boolean getEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}

	public List<Ator> getAtoresFiltrados() {
		return atoresFiltrados;
	}

	public void setAtoresFiltrados(List<Ator> atoresFiltrados) {
		this.atoresFiltrados = atoresFiltrados;
	}
	
	public List<Ator> completaAtores(String prefixo){
		return atorService.filter(prefixo);
	}
}
