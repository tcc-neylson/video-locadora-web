package org.neylson.videolocadora.converters;

import javax.faces.convert.FacesConverter;

import org.neylson.videolocadora.entidades.Diretor;

@FacesConverter("diretorConverter")
public class DiretorConverter extends EntityConverter<Diretor>{

	public DiretorConverter() {
		super(Diretor.class);
	}

}
