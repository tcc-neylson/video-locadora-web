package org.neylson.videolocadora.converters;

import javax.faces.convert.FacesConverter;

import org.neylson.videolocadora.entidades.Ator;

@FacesConverter("atorConverter")
public class AtorConverter extends EntityConverter<Ator>{

	public AtorConverter() {
		super(Ator.class);
	}

}
