package org.neylson.videolocadora.converters;

import java.lang.reflect.InvocationTargetException;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.neylson.videolocadora.entidades.PersistentEntity;

public class EntityConverter<T> implements Converter {

	private Class<T> entityClass;
	
	public EntityConverter(Class<T> entityClass) {
		this.entityClass = entityClass;
	}
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Long id = Long.valueOf(value);
		
		PersistentEntity entity = null;
		try {
			entity = (PersistentEntity) entityClass.getConstructor().newInstance();
			entity.setId(id);
			
			return entity;
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		PersistentEntity entity = (PersistentEntity) value;
		
		return entity.getId().toString(); 
	}

	
}
