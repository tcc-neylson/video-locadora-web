package org.neylson.videolocadora.converters;

import javax.faces.convert.FacesConverter;

import org.neylson.videolocadora.entidades.Filme;

@FacesConverter("filmeConverter")
public class FilmeConverter extends EntityConverter<Filme>{

	public FilmeConverter() {
		super(Filme.class);
	}

}
