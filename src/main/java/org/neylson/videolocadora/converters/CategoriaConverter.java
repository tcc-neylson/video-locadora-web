package org.neylson.videolocadora.converters;

import javax.faces.convert.FacesConverter;

import org.neylson.videolocadora.entidades.Categoria;


@FacesConverter("categoriaConverter")
public class CategoriaConverter extends EntityConverter<Categoria> {

	public CategoriaConverter() {
		super(Categoria.class);
	}

}
