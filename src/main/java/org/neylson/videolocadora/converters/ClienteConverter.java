package org.neylson.videolocadora.converters;

import javax.faces.convert.FacesConverter;

import org.neylson.videolocadora.entidades.Cliente;

@FacesConverter("clienteConverter")
public class ClienteConverter extends EntityConverter<Cliente>{

	public ClienteConverter() {
		super(Cliente.class);
	}

}
