package org.neylson.videolocadora.infra;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

// Usa escopo no nível de aplicação do CDI
@ApplicationScoped
public class MessagesHelper {

        // injeta uma instância de FacesContext
	@Inject
	private FacesContext facesContext;
	
	
        // Adicionar uma mensagem em facesContext
	public void addMessage(FacesMessage facesMessage) {
		facesContext.addMessage(null, facesMessage);
	}
}
